# Datasheet : FPGA Signal Acquisition and filtering

![Illustration](./Images/img_04.jpg){width=50%}

## Introduction

This project is a learning lab to acquire new skills in FPGA design and specially in VHDL. The goal is to design a signal acquisition and filtering system in VHDL, and specifically my goal is to design the interface used for acquisition and restitution of the signal.

## Summary

- [Introduction](#introduction)
- [Summary](#summary)
- [Hardware](#hardware)
- [Protocols](#protocols)
- [Architecture](#architecture)
- [implementation](#implementation)
- [Results](#results)


## Hardware

The hardware used for this project is a Digilent Zybo Z7-20 board based on a Xilinx Zynq-7000 SoC. The board is equipped with a Xilinx Zynq-7000 XC7Z020-1CLG400C SoC, since the project is hardware only design we won't use the ARM Cortex-A9 processor. Furthermore, the performances of the Zynq-7000 are more than enough for this project, so we won't have to worry too much about the size of the design.

Conveniently, the Zybo Z7 comes with 4 PMOD connectors, an interface used by Digilent to connect peripherals to the board. So, we will be using 2 PMOD peripherals : 

- The Pmod AD5, a 4-channel 24-bit low noise ADC with a max sampling rate of 4.8 ksps.
- The Pmod DA3, a single channel 16-bit DAC.

They both use the SPI interface to communicate with the FPGA, but they have a different protocol behind the SPI-layer. So, unlike in embedded software where we make use of the SPI library, we will have to implement the protocol ourselves. But since microcontrollers are not used in this project, we will also have to design the physical layer of the SPI interface.

# Protocols

## SPI

The SPI protocol is a serial communication protocol used by a controller to communicate with its peripherals. It comports 4 wires :
- SCLK : Serial Clock
- COPI : Controller Out Peripheral In 
- CIPO : Controller In Peripheral Out
- CS : Chip Select

It is a synchronous protocol, meaning that the clock signal is used to synchronize the data transfer, the clock signal is generated by the controller and is characterized by a frequency, a polarity and the phase.

In this project, the ADC uses a clock with a polarity of 1 meaning that the clock signal is high when idling, and a phase of 1 meaning that data is sent on the rising edge of the clock signal. Unlike the ADC, the DAC uses a clock with a polarity of 0 and a phase of 0, meaning that if we want to use the same clock signal for both peripherals, we will have to take care of changing the polarity and the phase of the clock signal.

The controller initiate the communication by pulling the CS signal low, then it sends the data to the peripheral by sending the data bit by bit on the COPI signal, and the peripheral sends the data to the controller by sending the data bit by bit on the CIPO signal. Only the controller can initiate the communication, and the peripheral can only respond to the controller. But since the SPI is a full duplex protocol, the controller can send data and receive at the same time.

## ADC

![ADC](./Images/ADC_00.png){width=25%}

The AD7193 is a 4-channel 24-bit low noise ADC with a max sampling rate of 4.8 ksps. It comports 2 fast settling filter types, a 50 Hz and 60 Hz notch filter, a gain parameter allowing to configure the sampling amplitude from 20mV to 2.5V. This analog to digital converter is mainly used in applications where high precision and low noise are required, such as metrology, medical and scientific instruments, Spatial, Aeronautic and automobile.

Since the initial goal of the project was to acquire and filter an audio signal, the ADC is less suited for this application due to its low sampling rate. But since the ADC is already available, we will configure it to the maximum sampling frequency of 4.8 ksps.

In order to configure the AD7193, we have to modify the 9 internal registers :
- The first register is the communication register, it is used to select the operation mode (read or write) and the register address.
- The second register is the status register, it is used to check the status of the ADC.
- The third register is the mode register, it is used to configure conversion mode, filter type, gain, etc.
- The fourth register is the configuration register, it is used to select the channel, the reference, the polarity, etc.
- The fifth register is the data register, it is used to store the data from the ADC.
- The sixth register is the ID register, it contains a unique identification number for the AD7193.
- The seventh register is the GPOCON register, the AD7193 has 4 general purpose outputs, this register is used to enable and set them to high or low.
- The height and ninth register are the offset and full-scale calibration registers, inside them are stored the calibration data.

A typical communication frame with the AD7193 looks like this :
- 8 bits of data are sent to the communication register to select the operation mode and the register address.
- 24 bits are then sent or received from the selected register.

For more information about the AD7193, you can check the datasheet [here](https://www.analog.com/media/en/technical-documentation/data-sheets/AD7193.pdf).

![AD7193](./Images/Registers.png){width=75%}

But in our case, we need to disable most of the features of the ADC to achieve the maximum sampling rate. So, we only need to modify a low number of registers. To simplify the explanation, we won't describe the complete configuration frames, but only the data sent to the registers :

- Firstly, we send two 32 bits words of only 1s to the COPI signal, this will trigger the AD7193 to reset its internal registers.
- Then, we send the data to the communication register to select the write operation and the configuration register. Followed by the 24 bits of configuration data : 0x000118 which means that we select the channel 1, the reference is internal, the polarity is unipolar, the gain is 1 and the buffer is enabled.
- It is necessary to configure the configuration register before calibration, so now we can modify the mode register to select the internal zero scale calibration and set the highest data output rate : 0x880001.
- After 500us, we can write again to the mode register for the internal full scale calibration : 0xA80001.

Now that the calibration is done, we can configure the ADC to the continuous conversion mode : 0x180001. The ADC will now acquire a new sample when we read the data register, to inform us that a new conversion is available, the ADC will set the RDY bit of the status register to 1 and set the CIPO signal to low.

To summarize, we can draw the following state machine :

![State Machine](./Images/StateMachine_00.png){width=75%}

During the project, we wrote an Arduino driver to communicate with the ADC, you can find it [here](/utils/AD7193/AD7193.ino). It was very useful to test the ADC and understand its protocol, it also helped us debug the VHDL driver since we could compare the waveforms generated by the Arduino and the ones generated by the simulations.

## DAC

![DAC](./Images/DAC_00.png){width=25%}

On the other hand, the DAC is much simpler to communicate with, it doesn't have any register. We only need to send the 16 bits digital value over the COPI signal to the DAC, and to pull low the CIPO (renamed as LDAC for Load DAC) signal to inform the DAC that the data is ready to be converted.

![DAC_Frame](./Images/Frame_00.png){width=75%}

On a side note, by sending bits to the COPI signal, each bit will be saved in an internal shift register, and when the LDAC signal is pulled low, the data will be converted to an analog signal. So, if we send more than 16 bits, only the 16 LSB will be converted to an analog signal, inversely, if we send less than 16 bits, the DAC will use the previous data to fill the missing bits. If we want to work with less than 16 bits it means that we have to pad the signal with 0s.

# Architecture

In order to achieve the goal of the project, we have to design a system that can :

- Implement the SPI protocol.
- Communicate with the ADC.
- Filter the audio signal.
- Communicate with the DAC.

Each of these tasks has to be done by a different VHDL module, and the modules have to be connected together to form a system. So, in order to keep everything synchronized, we used a clock master component to generate a 4kHz signal that will be used by all the modules.

For the ADC, a rising edge of the clock signal will trigger a new conversion, for the filter it means that the new sample can be read, and the previous one can be sent to the DAC. For the DAC, a rising edge of the clock signal will trigger a transmission of the new sample.

Then, we can draw the following architecture :

![Architecture](./Images/Architecture_00.png){width=75%}

# Implementation

## SPI Controller

The SPI protocol is complex to implement, we have to take care of the timing, the data format, the clock polarity and phase, etc. So, we decided to use an [SPI Master](https://forum.digikey.com/t/spi-master-vhdl/12717) component from Digikey in our design. Its simplicity and flexibility made it a good choice for our project since it can be adapted to our needs, furthermore, it is well documented, so we could easily understand how it works.

The SPI Master component starts a new transaction when the enable port is set to 1, it then sends the data from the data port to the slave device, and in parallel it receives the data from the slave device. A busy signal is set to 1 when the transaction is in progress, and it is set to 0 when the transaction is done. The SPI Master component also has a clock divider, so we can set the clock frequency of the SPI bus.

By using this component, we were able to focus mainly on the logic of the system, and we didn't have to worry about the timing of the SPI protocol.

## ADC

The ADC is the most complex module of the system, it has to implement the AD7193 protocol, and it has to be able to read and write the internal registers. The ADC module is composed of two submodules : the SPI controller and the AD7193 driver.

Since we are using the SPI Master module we have to instantiate it, it is configured to use 32 bits packets with a clock polarity and phase of 1 :

```vhdl
m_spi_master : spi_master
    GENERIC MAP (
        slaves  => 1,
        d_width => 32
    )
    PORT MAP (
        clock   => clk,
        reset_n => rst,
        enable  => enable,
        cpol    => '1',
        cpha    => '1',
        cont    => '0',
        clk_div => spi_clk_div,
        addr    => 0,
        tx_data => txdata,
        miso    => adc_miso,
        sclk    => adc_clk,
        ss_n    => adc_cs,
        mosi    => adc_mosi,
        busy    => busy,
        rx_data => rxdata
    );
```
In order to replicate the state machine defined in the previous sections, we have to implement a state machine in the ADC module. The state machine is composed of 7 states :

- **Reset** : 64 bits of 1s are sent to the ADC to reset its internal registers. Then, we wait for 500us.
- **Config** : The configuration register is set to 0x000118.
- **Calib** : The mode register is set to 0x880001 for zero scale calibration, then we wait for 500us, and finally we set the mode register to 0xA80001 and let 500us pass.
- **Init** : The mode register is set to 0x180001 to enable the continuous conversion mode. 
- **Waiting** : The module is waiting for a synchronization signal from the clock master.
- **Request** : Once a rising edge of the sync_clock signal is detected, the module sends a request to the ADC to read the data register. We are informed that the conversion is done when the CIPO signal is pulled low by the AD7193.
- **Read** : The module reads the data register and updates the output bus, the state machine then goes back to the waiting state.

To implement the delays of 500us, we used a counter that is incremented each time the state machine is in the waiting state, and when the counter reaches 500us, the state machine goes to the next state :

```vhdl
-- Wait 500 us to let the AD7193 reset
ELSIF (counter < 62500) THEN
    enable <= '0';
    counter := counter + 1;
ELSIF (counter = 62500) THEN
    counter := 0;
    state <= config;
END IF;
```

After sending data over SPI we need to wait for a falling edge of the busy signal to know that the transaction is done, so we have to delay the busy signal and when the delayed busy is high but the busy is low, we know that the transaction is done :

```vhdl
-- Reset the AD7193 by sending 64 1's
IF (counter < 2) THEN
    IF (busy = '0' AND s_busy = '0') THEN
        txdata <= (OTHERS => '1');                            
        enable <= '1';                            
    ELSIF (busy = '0' AND s_busy = '1') THEN
        counter := counter + 1;
    END IF;
```

The same logic is used to detect a rising edge of the synchronization clock signal :

```vhdl
WHEN waiting =>
        IF (synclk = '1' AND s_synclk = '0') THEN
            state <= request;
        END IF;
```

## DAC

The DAC module is also based on the SPI Master component, it is configured to use 16 bits packets with a clock polarity and phase of 0 :

```vhdl
    -- Instantiate the spi_master component
    m_spi_master : spi_master
    GENERIC MAP (
        slaves  => 1,
        d_width => N
    )
    PORT MAP (
        clock   => i_clk,
        reset_n => i_rst_n,
        enable  => s_ena,
        cpol    => '0',
        cpha    => '0',
        cont    => '0',
        clk_div => spi_clk_div,
        addr    => 0,
        tx_data => s_reg,
        miso    => '0',
        sclk    => o_spi_clk,
        ss_n    => o_spi_cs_n,
        mosi    => o_spi_din,
        busy    => s_busy,
        rx_data => open
    );
```

Similarly to the ADC module, we used counters to implement the delays and delayed signals to detect the rising and falling edges.

The state machine comports only 3 states :

- **Init** : Use the counter to wait for 100us before going to the next state. The AD5541A needs this time to power up.
- **Waiting** : The module is waiting for a synchronization signal from the clock master.
- **Sending** : Send the data to the DAC using the SPI Master component.
- **Pause** : Use the counter to wait for 5us, set LDAC to low, wait for 10us, set LDAC to high, and go back to the waiting state after an additional 5us.

# Results

## ADC

The Pmod AD7193 was first tested with an Arduino board, and then with the FPGA. The first step was to verify that we were able to read the data register of the ADC.

For this, we used an oscilloscope to monitor the transmission of the data register over SPI, and a signal generator to create a signal that would be converted by the ADC. The signal generator was set to 1 Hz.

![img_00](./Images/img_00.jpg){width=75%}

As a result, we determined that the AD7193 was able to convert the signal and send the data over SPI. The next step was to verify that we were able to set a high frequency sampling rate.

But when configuring the ADC, we noticed that we were not able to send or receive data over SPI. We tried to change the Arduino code, but the problem was coming from the breadboard and the wires electrical connections between the Arduino and the Pmod AD7193. The packets were not sent correctly, and the ADC would reconfigure itself according to the corrupted data. Thus resulting in the ADC crashing and not being able to send or receive data.

Once the problem was identified, we decided to use a prototype board to connect the Pmod AD7193 to the Arduino. This solved the problem, and we were able to sample the signal at 4.8 kHz :

![img_03](./Images/img_03.jpg){width=75%}

The next step was to implement it in FPGA, but for verification purposes we wrote a test bench to simulate the ADC's answer, we encode an increasing value bits by bits and send it on the CIPO signal. On the following image, we can see that the data is correctly read by the ADC module :

![img_05](./Images/img_05.png)


## DAC

The Pmod AD5541A wasn't tested with an Arduino board, since we were directly able to test it with the FPGA. The first step was to verify the behavior with simulations, like the ADC module, we encode a value on the input bus of the design. The only verification we can do at this point is to check that the SPI trams are correctly sent to the DAC.

![img_06](./Images/img_06.png)

The next step was to test the DAC on real hardware. We connected the 4 switches of the Zybo Z7 to the 4 MSB of the input bus of the DAC module. Using a multimeter or an oscilloscope, we can see that the DAC is able to output a voltage that is proportional to the value coded on the switches.

## Top level

The top level of the design is composed of the ADC and DAC modules, the clock master and the clock slave. But due to a lack of time, we were not able to implement the filter in the top level. The filter was implemented in a separate design and not included in the final architecture since we would still need to validate the ADC and DAC modules together.

![img_07](./Images/img_07.png){width=75%}