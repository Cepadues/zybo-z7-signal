# Zybo Z7 Signal acquisition and processing

## Dependencies 

This project was made with Vivado 2022.2 and has not been tested with older releases.

The target device is a Zybo Z7-20 equipped with a Zynq-7000 : xc7z020clg400-1. Please, make sure you have already installed the corresponding board file : [Digilent Reference](https://digilent.com/reference/programmable-logic/guides/installing-vivado-and-vitis#install_digilent_s_board_files).

## Installation

To create the Vivado project, clone the repository :

```
git clone https://gitlab.com/Cepadues/zybo-z7-signal.git
```
And from Vivado's main window run `build.tcl` script included with this project :

![installation](./docs/Images/Installation_01.png)

This script will create the project for you without copying the source files from `src/`, so any modification made by you to the files under `src/` will be reflected in Vivado.

## Usage

The VHDL code targets the Zybo Z7-20 board and is designed to be used with the [PMOD AD5]() and [PMOD DA3]() from Digilent.

They must be plugged to the JB and JC PMOD connectors respectively. Make sure to turn off the power supply before plugging them, and that the ADC is to the left of the DAC.

If you want to change the PMOD used, you must change the constraint file and the top.vhd accordingly.

You can change the sampling frequency of the filter by adjusting the clocky.vhd file. The default value is 4 kHz.

## Project arborescence

```
.
├── build.tcl
├── docs
│   ├── Images
│   └── README.md
├── ips
│   ├── xadc_wiz_0.xci
│   └── design.txt
├── src
│   ├── constraints
│   │   └── zybo-z7.xdc
│   ├── design
│   │   ├── adc.vhd
│   │   ├── clocky.vhd
│   │   ├── dac.vhd
│   │   ├── spi_controller.vhd
│   │   ├── top.vhd
│   │   └── xadc.vhd
│   └── testbench
│       ├── adc_tb.vhd
│       ├── clocky_tb.vhd
│       ├── dac_tb.vhd
│       └── top_tb.vhd
└── utils
    ├── AD7193
    │   └── AD7193.ino
    ├── scripts
    │   ├── git_wrapper.tcl
    │   └── write_project_tcl_git.tcl
    └── Vivado_init.tcl
```

This repository contains the following folders :

- `docs/` : contains the documentation of the project.
- `ips/` : contains the IP cores, in the end we didn't use them.
- `src/` : contains the source files of the project : Constraint file, vhdl components and testbenches.
- `utils/` : contains the scripts used to create the build script and the Arduino driver used to test the ADC and its configurations.

The architecture of the FPGA consists of a top module which instantiates the ADC, the DAC and the clock generator. The ADC and the DAC are both based on the SPI protocol and are controlled by a SPI controller from Digikey.

## Contributing

If you made any change to the sources, you must also update the build script.
For that, copy the content of the `utils\` folder to `~\.Xilinx\Vivado`.

If `Vivado_init.tcl` already exists, append the content of the new file to the old file.

"Fork" of : [vivado-git](https://github.com/barbedo/vivado-git).

This will create the `wproj` command which can be used to create a build script from the current project.

To create a new module, you must place it under the corresponding folder in `src\` and use the "add sources" button from Vivado. Make sure to untick `copy sources into project`.

Finally, once you're ready to commit your changes, open a TCL console from Vivado and run `wproj`. Then, follow the standard git workflow to upload the update.
