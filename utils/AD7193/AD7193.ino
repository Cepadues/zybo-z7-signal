/*
-----------------------------------------------------
 Signal Name |  PmodAD Pin  |  Arduino Pin
-----------------------------------------------------
  ~CS        |  J1, Pin 1   |  10
  MOSI       |  J1, Pin 2   |  MOSI or 11 or ICSP-4
  MISO       |  J1, Pin 3   |  MISO or 12 or ICSP-1
  SCLK       |  J1, Pin 4   |  SCLK or 13 or ICSP-3
  GND        |  J1, Pin 5   |  GND
  VCC (3.3V) |  J1, Pin 6   |  3.3V
-----------------------------------------------------

SPI_MODE3 : CPOL = 1, CPHA = 1, Output on falling edge, input on rising edge
MSB_FIRST : Most significant bit first

-----------------------------------------------------
AD7193 Register Map
-----------------------------------------------------
Address | Name | Type | Size | Description
-----------------------------------------------------
 000    | COMM | W    | 8    | Communications Register
 000    | STAT | R    | 8    | Status Register
 001    | MODE | RW   | 24   | Mode Register
 010    | CONF | RW   | 24   | Configuration Register
 011    | DATA | R    | 24   | Data Register
 100    | ID   | R    | 8    | ID Register
 101    | GPO  | RW   | 8    | GPOCON Register
 110    | OFFS | RW   | 24   | Offset Register
 111    | FULL | RW   | 24   | Full-Scale Register
-----------------------------------------------------
*/
#include <SPI.h>

#define CS_PIN 10

#define AD7193_COMM_REG   0x00
#define AD7193_STATUS_REG 0x00
#define AD7193_MODE_REG   0x01
#define AD7193_CONF_REG   0x02
#define AD7193_DATA_REG   0x03
#define AD7193_ID_REG     0x04
#define AD7193_GPOCON_REG 0x05
#define AD7193_OFFS_REG   0x06
#define AD7193_FULL_REG   0x07

void setup()
{
  // Initialize serial communication
  Serial.begin(1000000);  // Étonnant, mais ça fonctionne

  // Initialize SPI communication
  SPI.begin();
  SPI.setClockDivider(SPI_CLOCK_DIV16);
  SPI.setDataMode(SPI_MODE3);
  pinMode(CS_PIN, OUTPUT);
  digitalWrite(CS_PIN, HIGH);

  // Give the AD7193 some time to power up
  delay(500);
  AD7193_Reset();
  AD7193_setGain(1);
  AD7193_setFiltering(1);
  AD7193_Calibration();
}

/*
  Function: AD7193_Reset
  Reset the AD7193 by sending 40 consecutive 1's
  Parameters:
    void
  Returns:
    void
*/
void AD7193_Reset()
{
  Serial.println("Reset");
  
  // Set the ~CS pin low to start the SPI communication
  digitalWrite(CS_PIN, LOW);

  // We need to send 40 consecutive 1's to reset the AD7193
  // 40 / 8 = 5 bytes
  for (int i = 0; i < 6; i++)
  {
    SPI.transfer(0xFF);
  }

  // Set the ~CS pin high to end the SPI communication
  digitalWrite(CS_PIN, HIGH);

  // Give the AD7193 some time to reset (datasheet p38)
  delay(500);
}

/*
  Function: AD7193_setGain
  Setup the AD7193's gain
  Parameters:
    unsigned char gain - the gain to set the AD7193 to.
      The gain must be one of the following values:
        1, 8, 16, 32, 64, 128
  Returns:
    void
*/
void AD7193_setGain(unsigned char gain)
{
  // Read the current value of the configuration register
  unsigned long configurationRegister = AD7193_ReadRegister(AD7193_CONF_REG, 3);
  Serial.print("Setting gain, current configuration register value: ");
  Serial.println(configurationRegister, HEX);

  // Set the ADC to unipolar mode
  // Set the Polarity bit to 1 (Bit 3)
  configurationRegister |= 0x08;

  // Clear the gain bits (3 LSBs)
  configurationRegister &= 0xFFFFF8;

  /*
     Gain | Bits
    ------|-----
     1    | 000
     8    | 011
     16   | 100
     32   | 101
     64   | 110
     128  | 111
  */

  // Set the gain bits
  switch (gain)
  {
  case 1 :
    configurationRegister |= 0x00;
    break;
  case 8 :
    configurationRegister |= 0x03;
    break;
  case 16 :
    configurationRegister |= 0x04;
    break;
  case 32 :
    configurationRegister |= 0x05;
    break;
  case 64 :
    configurationRegister |= 0x06;
    break;
  case 128 :
    configurationRegister |= 0x07;
    break;
  default:
    // Default to 1
    configurationRegister |= 0x00;
    break;
  }

  // Output the new configuration register value
  Serial.print("New Configuration Register: ");
  Serial.println(configurationRegister, HEX);

  // Write the new configuration register value
  AD7193_WriteRegister(AD7193_CONF_REG, configurationRegister, 3);
}

/*
  Function: AD7193_setFiltering
  Write bit FS9 to FS0 to the mode register to set the output rate
  Parameters:
    unsigned char datarate - the filter to set the AD7193 to.
      The datarate must been within the range of 1 to 1023.
      A value of 1 will give the fastest output rate, while a value of 1023 will give the slowest output rate.
  Returns:
    void
*/
void AD7193_setFiltering(unsigned char datarate)
{
    // Read the current value of the mode register
  unsigned long modeRegister = AD7193_ReadRegister(AD7193_MODE_REG, 3);
  Serial.print("Setting filtering, current mode register value: ");
  Serial.println(modeRegister, HEX);

  // We want to completely disable the fast settling filter
  // Clear the fast settling bits (bit 17 and 16)
  modeRegister &= 0xFCFFFF;

  // Set the sync filter to sync 4 mode
  // Clear the sync filter bits (bit 15)
  modeRegister &= 0xFF7FFF;

  // Clear the datarate bits (10 LSBs)
  modeRegister &= 0xFFFC00;

  if (datarate > 1023)
  { 
    Serial.println("Invalid datarate, setting to 1023");
    datarate = 1023;
  }
  else if (datarate < 1)
  {
    Serial.println("Invalid datarate, setting to 1");
    datarate = 1;
  }

  // Set the datarate bits
  modeRegister |= datarate;

  // Output the new mode register value
  Serial.print("New Mode Register: ");
  Serial.println(modeRegister, HEX);

  // Write the new mode register value
  AD7193_WriteRegister(AD7193_MODE_REG, modeRegister, 3);
}

/*
  Function: AD7193_Calibration
  Callibrate the AD7193 by sending a Calibration command to the mode register
  First Zero-scale Calibration is performed, then Full-scale.
  Parameters:
    void
  Returns:
    void
*/
void AD7193_Calibration()
{
  Serial.println("Callibrating : Zero-scale");

  // Read the current value of the mode register
  unsigned long modeRegister = AD7193_ReadRegister(AD7193_MODE_REG, 3);

  // Clear the mode bits (3 MSBs)
  modeRegister &= 0x1FFFFF;

  // Set the mode bits to callibrate zero-scale
  modeRegister |= 0x800000;

  // Write the new mode register value
  AD7193_WriteRegister(AD7193_MODE_REG, modeRegister, 3);

  // Wait for the Calibration to complete
  delay(1000);

  Serial.println("Callibrating : Full-scale");

  // Clear the mode bits (3 MSBs) (again)
  modeRegister &= 0x1FFFFF;

  // Set the mode bits to callibrate full-scale
  modeRegister |= 0xA00000;

  // Write the new mode register value
  AD7193_WriteRegister(AD7193_MODE_REG, modeRegister, 3);

  // Wait for the Calibration to complete
  delay(1000);

  // Set the mode bits to continuous conversion
  modeRegister &= 0x1FFFFF;
  Serial.print("Setting continuous conversion mode : ");
  Serial.println(modeRegister, HEX);

  // Write the new mode register value
  AD7193_WriteRegister(AD7193_MODE_REG, modeRegister, 3);
}

/*
  Function: AD7193_WaitForReady
  Wait for the RDY pin (MISO) to go low
  Parameters:
    void
  Returns:
    char : 1 if the RDY pin went low, 0 if the RDY pin never went low
*/
char AD7193_WaitForReady()
{   
    int breakTime = 0;
    char res = 0;

    //Serial.println("Waiting for Conversion");

    while(1){
      if (digitalRead(12) == 0){      // Break if ready goes low
        res = 1;
        break;
      }

      if (breakTime > 5000) {                       // Break after five seconds - avoids program hanging up
        Serial.println("Data Ready never went low!");
        break;
      }

      breakTime = breakTime + 1;
    }

    return res;
}

/*
  Function: AD7193_WriteRegister
  Write the value of a register from the AD7193
  Parameters:
    registerAddress - the address of the register to read from.
    registerValue   - the value to write to the register.
  Returns:
    void
*/
void AD7193_WriteRegister(unsigned char registerAddress, unsigned long registerValue, unsigned char numberOfBytes)
{
  // Set the ~CS pin low to start the SPI communication
  digitalWrite(CS_PIN, LOW);

  // Create the communication word to send to the AD7193
  unsigned char communicationWord = 0x00;
  communicationWord |= (registerAddress << 3);
  SPI.transfer(communicationWord);
  // Send the register value to the AD7193
  for (int i = 0; i < numberOfBytes; i++)
  {
    SPI.transfer((registerValue >> ((numberOfBytes - 1 - i) * 8)) & 0xFF);
  }

  // Set the ~CS pin high to end the SPI communication
  digitalWrite(CS_PIN, HIGH);
}

/*
  Function: AD7193_ReadRegister
  Read the value of a register from the AD7193
  Parameters:
    registerAddress - the address of the register to read from.
    numberOfBytes   - the number of bytes to read from the register.
  Returns:
    unsigned long - the value of the register.
*/
unsigned long AD7193_ReadRegister(unsigned char registerAddress, unsigned char numberOfBytes)
{

  unsigned char received = 0;
  unsigned long buffer   = 0;

  // Set the ~CS pin low to start the SPI communication
  digitalWrite(CS_PIN, LOW);

  // Create the communication word to send to the AD7193
  unsigned char communicationWord = 0x00;
  communicationWord |= (registerAddress << 3);
  communicationWord |= (1 << 6);  // Set the READ bit
  SPI.transfer(communicationWord);

  // Wait for ADC to be ready 
  char res = AD7193_WaitForReady();

  // Read the register value from the AD7193
  for (int i = 0; i < numberOfBytes; i++)
  {
    received = SPI.transfer(0x00);
    buffer = (buffer << 8) | received;
  }

  // Set the ~CS pin high to end the SPI communication
  digitalWrite(CS_PIN, HIGH);

  return buffer;
}

/*
  Function: AD7193_ToVoltage
  Convert the raw ADC value to a voltage
  Parameters:
    rawValue - the raw ADC value to convert.
  Returns:
    float - the voltage value.
*/
float AD7193_ToVoltage(unsigned long rawValue, int gain)
{
  /*
    See page 33 of the AD7193 datasheet for more information on the conversion formula
  */
  return (float)rawValue * (2.5 / (16777216 * gain));
}


void loop()
{
  demo_reg();
  //unsigned long registerValue = AD7193_ReadRegister(AD7193_DATA_REG, 3);
  //Serial.println(AD7193_ToVoltage(registerValue, 1), HEX);
}

/*
  Function: demo_reg
  Demo function to read the value of a register from the AD7193
  Parameters:
    void
  Returns:
    void
*/
void demo_reg()
{
  // Wait for user input on the serial monitor
  // Read the value of the register corresponding to the user input
  // Print the value of the register to the serial monitor
  if (Serial.available() > 0)
  {
    char input = Serial.read();
    unsigned long registerValue = AD7193_ReadRegister(input - 0x30, 3);
    Serial.print("Register");
    Serial.print(input - 0x30, HEX);
    Serial.print(" : ");
    Serial.println(registerValue, HEX);
  }
}