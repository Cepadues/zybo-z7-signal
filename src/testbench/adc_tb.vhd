----------------------------------------------------------------------------------
-- Company: GPSE
-- Engineer: Julien Chapel
-- Create Date: 12/06/2022 10:50:25 AM
-- Design Name: ADC_tb controller
-- Module Name: ADC_tb - Behavioral
-- Project Name: Zybo Dac
-- Project Name: Projet TP
-- Target Devices: Zybo Z7
-- Description: 
--   This is a tesbt bench for the ADC
----------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY ADC_tb IS
END ADC_tb;

ARCHITECTURE Behavioral OF ADC_tb IS

    SIGNAL clk      : STD_LOGIC := '0';
    SIGNAL rst      : STD_LOGIC := '0';
    SIGNAL syncclk  : STD_LOGIC := '0';
    SIGNAL data     : STD_LOGIC_VECTOR(23 DOWNTO 0);
    
    SIGNAL buff     : STD_LOGIC_VECTOR(32 DOWNTO 0);

    SIGNAL adc_cs   : STD_LOGIC_VECTOR(0 DOWNTO 0);
    SIGNAL adc_miso : STD_LOGIC := '0';
    SIGNAL adc_mosi : STD_LOGIC := '0';
    SIGNAL adc_clk  : STD_LOGIC := '0';

    -- Clock periods
    CONSTANT clk_period      : TIME := 8 ns;
    CONSTANT sync_clk_period : TIME := 250 us;

    COMPONENT ADC is
    GENERIC (
        spi_clk_div : INTEGER           -- SPI clock divider
    );

    PORT (
        clk      : IN STD_LOGIC;                        -- System clock
        rst      : IN STD_LOGIC;                        -- Reset
        synclk   : IN STD_LOGIC;                        -- Synchronization clock
        adc_cs   : BUFFER STD_LOGIC_VECTOR(0 DOWNTO 0); -- Chip select
        adc_clk  : BUFFER STD_LOGIC;                    -- SPI clock
        adc_mosi : OUT STD_LOGIC;                       -- SPI MOSI
        adc_miso : IN STD_LOGIC;                        -- SPI MISO
        adc_data : OUT STD_LOGIC_VECTOR (23 DOWNTO 0)   -- ADC data
    );
    END COMPONENT;

BEGIN

    -- ADC instantiation
    UUT : ADC
    GENERIC MAP (
        spi_clk_div => 64
    )
    PORT MAP (
        clk      => clk,
        rst      => rst,
        synclk   => syncclk,
        adc_cs   => adc_cs,
        adc_clk  => adc_clk,
        adc_mosi => adc_mosi,
        adc_miso => adc_miso,
        adc_data => data
    );

    -- Reset
    rst <= '1' after 128 ns;

    -- System clock process 
    clk_process : PROCESS
    BEGIN
        clk <= '0';
        WAIT FOR clk_period/2;
        clk <= '1';
        WAIT FOR clk_period/2;
    END PROCESS clk_process;

    -- Synchronization clock process
    syncclk_process : PROCESS
    BEGIN
        syncclk <= '0';
        WAIT FOR sync_clk_period/2;
        syncclk <= '1';
        WAIT FOR sync_clk_period/2;
    END PROCESS syncclk_process;

    -- ADC output
    -- Encode the value sent by MISO on the spi_clk
    miso_process : PROCESS(adc_clk, syncclk, adc_mosi)
        VARIABLE i : INTEGER := 0;
        VARIABLE val : INTEGER := 0;
    BEGIN
        -- increment the encoded value
        IF RISING_EDGE(syncclk) THEN
            i := 0;
            val := val + 1;
            buff <= STD_LOGIC_VECTOR(TO_UNSIGNED(val, buff'length));
        END IF;
        
        -- If the spi clock is falling edge
        IF (adc_clk'EVENT AND adc_clk = '0' AND syncclk = '1') THEN
            IF (i > 31) THEN
                adc_miso <= buff(63 - i);
            END IF;
            i := i + 1;
        END IF;
        
        IF (adc_mosi = 'Z') THEN
            adc_miso <= '0';
        END IF;
        
    END PROCESS miso_process;
END Behavioral;