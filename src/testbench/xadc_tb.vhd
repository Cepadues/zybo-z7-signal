----------------------------------------------------------------------------------
-- Company: GPSE
-- Engineer: Julien Chapel
-- Create Date: 12/06/2022 10:51:02 AM
-- Design Name: ADC controller
-- Module Name: ADC_tb - Behavioral
-- Project Name: Zybo Dac
-- Project Name: Projet TP
-- Target Devices: Zybo Z7
-- Description: 
--   This is a driver to control the ADC
----------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY ADC_tb IS
END ADC_tb;

ARCHITECTURE Behavioral OF ADC_tb IS

    -- Stimulus signal
    SIGNAL clk      : STD_LOGIC;
    SIGNAL rst      : STD_LOGIC := '0';
    SIGNAL sync_clk : STD_LOGIC;
    SIGNAL data_out : STD_LOGIC_VECTOR(11 DOWNTO 0);
    SIGNAL ja       : STD_LOGIC_VECTOR(7 DOWNTO 0);

    -- Clock periods
    CONSTANT clk_period      : TIME := 8 ns;
    CONSTANT sync_clk_period : TIME := 250 us;

    -- ADC declaration
    COMPONENT ADC IS
        PORT (
            i_clk       : IN STD_LOGIC;                             -- Clock
            i_rst_n     : IN STD_LOGIC;                             -- Reset
            i_sync_clk  : IN STD_LOGIC;                             -- Synchronisation clock
            o_data      : OUT STD_LOGIC_VECTOR (11 DOWNTO 0);       -- Data output
            ja          : IN STD_LOGIC_VECTOR (7 DOWNTO 0)
        );
    END COMPONENT;

BEGIN

    -- ADC instantiation
    UUT : ADC
    PORT MAP (
        i_clk       => clk,
        i_rst_n     => rst,
        i_sync_clk  => sync_clk,
        o_data      => data_out,
        ja          => ja
    );

    -- Reset
    rst <= '1' after 16 ns;

    -- Clock process
    clk_process : PROCESS
    BEGIN
        clk <= '0';
        WAIT FOR clk_period / 2;
        clk <= '1';
        WAIT FOR clk_period / 2;
    END PROCESS clk_process;

    -- Synchronisation clock process
    sync_clk_process : PROCESS
    BEGIN
        sync_clk <= '0';
        WAIT FOR sync_clk_period / 2;
        sync_clk <= '1';
        WAIT FOR sync_clk_period / 2;
    END PROCESS sync_clk_process;

END Behavioral;