----------------------------------------------------------------------------------
-- Company: GPSE
-- Engineer: Julien Chapel
-- Create Date: 11/07/2022 06:49:47 PM
-- Design Name: 
-- Module Name: DAC_tb - Behavioral
-- Project Name: Zybo Dac
-- Project Name: Projet TP
-- Target Devices: Zybo Z7
-- Description: 
--   This is a testbench for the DAC module
----------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY DAC_tb IS
END DAC_tb;

ARCHITECTURE Behavioral OF DAC_tb IS

    -- Signals declaration
    SIGNAL i_clk_tb        : STD_LOGIC := '0';                                          -- Clock @ 125 MHz
    SIGNAL i_rst_n_tb      : STD_LOGIC := '0';                                          -- Reset active low
    SIGNAL i_sync_clk_tb   : STD_LOGIC := '0';                                          -- Sampling clock at 4 kHz
    SIGNAL i_data_tb       : STD_LOGIC_VECTOR (15 DOWNTO 0) := "0110011001100110";      -- Data to be converted
    SIGNAL o_spi_clk_tb    : STD_LOGIC;                                                 -- SPI clock
    SIGNAL o_spi_cs_n_tb   : STD_LOGIC_VECTOR(0 DOWNTO 0);                              -- SPI chip select
    SIGNAL o_spi_din_tb    : STD_LOGIC;                                                 -- SPI data in
    SIGNAL o_spi_ldac_n_tb : STD_LOGIC;                                                 -- SPI load DAC (MOSI)
        
    COMPONENT dac IS
        GENERIC (
            N            : INTEGER;                                 -- Resolution of the DAC
            spi_clk_div  : INTEGER                                  -- SPI Clock div, freq_spi = 125MHz / spi_clk_div
        );

        PORT ( 
            i_clk        :  IN STD_LOGIC;                           -- System clock
            i_rst_n      :  IN STD_LOGIC;                           -- Synchronous reset
            i_sync_clk   :  IN STD_LOGIC;                           -- Enable communication with DAC
            i_data       :  IN STD_LOGIC_VECTOR (N - 1 DOWNTO 0);   -- Data to be loaded in the DAC
            o_spi_clk    :  BUFFER STD_LOGIC;                       -- SPI clock
            o_spi_cs_n   :  BUFFER STD_LOGIC_VECTOR(0 DOWNTO 0);    -- SPI chip select
            o_spi_din    :  OUT STD_LOGIC;                          -- Data sent to the DAC, MOSI
            o_spi_ldac_n :  OUT STD_LOGIC                           -- Load DAC output with the loaded data, replace MISO port
        );
    END COMPONENT;

BEGIN

    -- Stimulus
    i_clk_tb   <= not i_clk_tb after 4 ns;                 -- System clock @ 125 MHz
    i_rst_n_tb <= '1' after 24 ns;                         -- Reset at start

    -- Process to generate the sync clock
    trans_process: PROCESS(i_clk_tb)                            -- Transaction Process
    
        VARIABLE counter : INTEGER RANGE 0 TO 31249 := 0;       -- 4 kHz counter
    BEGIN

        IF (rising_edge(i_clk_tb)) THEN                         -- Rising edge of the clock
            counter := counter + 1;                             -- Increment counter

            IF (counter = 31249) THEN                           -- 4 kHz
                i_sync_clk_tb <= not i_sync_clk_tb;             -- Toggle sync clock
                counter := 0;                                   -- Reset counter
            END IF;
        END IF;

    END PROCESS;
    
    -- Instantiate the DAC
    UUT : dac
    GENERIC MAP (
        N            => 16,                         -- Resolution of the DAC
        spi_clk_div  => 1024                        -- SPI Clock div, freq_spi = 125MHz / spi_clk_div
    )
    PORT MAP (
        i_clk        =>  i_clk_tb,                  -- System clock
        i_rst_n      =>  i_rst_n_tb,                -- Synchronous reset
        i_sync_clk   =>  i_sync_clk_tb,             -- Enable communication with DAC
        i_data       =>  i_data_tb,                 -- Data to be loaded in the DAC
        o_spi_clk    =>  o_spi_clk_tb,              -- SPI clock
        o_spi_cs_n   =>  o_spi_cs_n_tb,             -- SPI chip select
        o_spi_din    =>  o_spi_din_tb,              -- Data sent to the DAC, MOSI
        o_spi_ldac_n =>  o_spi_ldac_n_tb            -- Load DAC output with the loaded data, replace MISO port
    );
    
END Behavioral;