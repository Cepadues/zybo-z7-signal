----------------------------------------------------------------------------------
-- Company: GPSE
-- Engineer: Julien Chapel
-- Create Date: 12/01/2022 10:27:43 AM
-- Design Name: clocky
-- Module Name: clocky_tb - Behavioral
-- Project Name: Zybo Dac
-- Project Name: Projet TP
-- Target Devices: Zybo Z7

-- Description:
-- This testbench is used to test the clocky module
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY clocky_tb IS
END clocky_tb;

ARCHITECTURE Behavioral OF clocky_tb IS

    -- Component declaration for the unit under test
    COMPONENT clocky IS
        GENERIC (
            top_freq : INTEGER
        );
        PORT (
            i_clk   : IN STD_LOGIC;
            i_rst_n : IN STD_LOGIC;
            o_top   : OUT STD_LOGIC
        );
    END COMPONENT;

    -- Signals declaration
    SIGNAL clk   : STD_LOGIC := '0';
    SIGNAL rst_n : STD_LOGIC := '0';
    SIGNAL top   : STD_LOGIC;

BEGIN

    -- Instantiate the unit under test
    UUT : clocky 
    GENERIC MAP (
        top_freq => 4000
    )
    PORT MAP (
        i_clk   => clk,
        i_rst_n => rst_n,
        o_top   => top
    );

    -- Stimulus process (125 MHz clock)
    clk_process: PROCESS
    BEGIN
        clk <= '0';
        wait for 4 ns;
        clk <= '1';
        wait for 4 ns;
    END PROCESS clk_process;

    -- Reset
    rst_n <= '1' after 16 ns;
    
END Behavioral;
