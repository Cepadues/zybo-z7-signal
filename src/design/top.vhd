----------------------------------------------------------------------------------
-- Company: GPSE
-- Engineer: Julien Chapel
-- Create Date: 11/14/2022 09:51:33 AM
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: Zybo Dac
-- Project Name: Projet TP
-- Target Devices: Zybo Z7
-- Description:
-- This top level module aims to test physically the DAC.
-- We connect the 4 switch of the zybo to the 4 MSB of the Data bus.
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY top IS

  PORT (
    sysclk      : IN  STD_LOGIC;                        -- System clock from Zybo's K17 pin
    jb          : INOUT STD_LOGIC_VECTOR(3 DOWNTO 0);     -- JP Pmod connector
    jc          : INOUT STD_LOGIC_VECTOR(3 DOWNTO 0);     -- JA Pmod connector
    btn_rst     : IN  STD_LOGIC                         -- Reset button
  );

END top;

ARCHITECTURE Behavioral OF top IS

    -- Control Signals Declaration
    SIGNAL data_dac : STD_LOGIC_VECTOR(11 DOWNTO 0);    -- Data register for the DAC
    SIGNAL data_adc : STD_LOGIC_VECTOR(23 DOWNTO 0);    -- Data register for the ADC
    SIGNAL sync_clk : STD_LOGIC;                        -- Synchronization clock
    SIGNAL reset    : STD_LOGIC;

    -- DAc Signals Declaration
    SIGNAL dac_clk : STD_LOGIC;
    SIGNAL dac_cs  : STD_LOGIC_VECTOR(0 DOWNTO 0);

    -- ADC Signals Declaration
    SIGNAL adc_cs   : STD_LOGIC_VECTOR(0 DOWNTO 0);
    SIGNAL adc_clk  : STD_LOGIC;

    -- Clocky Component Declaration
    COMPONENT clocky IS
    GENERIC (
        target_freq : INTEGER                                   -- Frequency in Hz at which a new sample is generated, processed and sent.
    );
    PORT (
        i_clk   : IN STD_LOGIC;                                 -- Input clock
        i_rst_n : IN STD_LOGIC;                                 -- Reset signal : synchronous, active low
        o_sync_clk   : OUT STD_LOGIC                            -- Output signal used to synchronize the components together
    );
    END COMPONENT;
    
    -- DAC Component Declaration
    COMPONENT dac IS
    GENERIC (
        N            : INTEGER;                                 -- Resolution of the DAC
        spi_clk_div  : INTEGER                                  -- SPI Clock frequency (around 100kHz)
    );
    PORT (
        i_clk        :  IN STD_LOGIC;                           -- System clock
        i_rst_n      :  IN STD_LOGIC;                           -- Reset Asynchrone
        i_sync_clk   :  IN STD_LOGIC;                           -- Enable communication with DAC
        i_data       :  IN STD_LOGIC_VECTOR (N - 1 DOWNTO 0);   -- Data to be loaded in the DAC
        o_spi_clk    :  BUFFER STD_LOGIC;                       -- SPI clock
        o_spi_cs_n   :  BUFFER STD_LOGIC_VECTOR(0 DOWNTO 0);    -- SPI chip select
        o_spi_din    :  OUT STD_LOGIC;                          -- Data sent to the DAC, MOSI
        o_spi_ldac_n :  OUT STD_LOGIC                           -- Load DAC output with the loaded data, replace MISO port
    );
    END COMPONENT;

    -- ADC Component Declaration
    COMPONENT ADC IS
    GENERIC (
        spi_clk_div : INTEGER := 64     -- SPI clock divider
    );

    PORT (
        clk      : IN STD_LOGIC;                        -- System clock
        rst      : IN STD_LOGIC;                        -- Reset
        synclk   : IN STD_LOGIC;                        -- Synchronization clock
        adc_cs   : BUFFER STD_LOGIC_VECTOR(0 DOWNTO 0); -- Chip select
        adc_clk  : BUFFER STD_LOGIC;                    -- SPI clock
        adc_mosi : OUT STD_LOGIC;                       -- SPI MOSI
        adc_miso : IN STD_LOGIC;                        -- SPI MISO
        adc_data : OUT STD_LOGIC_VECTOR (23 DOWNTO 0)   -- ADC data
    );
    END COMPONENT;

BEGIN
    
    -- Spi clock and chip select
    jb(3) <= dac_clk;
    jb(0) <= dac_cs(0);
    jc(3) <= adc_clk;
    jc(0) <= adc_cs(0);

    -- Reset signal
    reset <= '1' WHEN btn_rst = '0' ELSE '0';

    -- Data bus
    data_dac <= data_adc(23 DOWNTO 12);
    
    -- Device Instantation
    m_dac : dac
    GENERIC MAP (
        N            => 12,             -- Resolution of the DAC
        spi_clk_div  => 512                           
    )
    PORT MAP (
        i_clk        =>  sysclk,
        i_rst_n      =>  reset,
        i_sync_clk   =>  sync_clk,    
        i_data       =>  data_dac,  
        o_spi_clk    =>  dac_clk,
        o_spi_cs_n   =>  dac_cs,
        o_spi_din    =>  jb(1),
        o_spi_ldac_n =>  jb(2)
    );

    -- Clocky Component Declaration
    m_clocky : clocky
    GENERIC MAP (
        target_freq => 4000             -- Frequency in Hz at which a new sample is generated, processed and sent.
    )
    PORT MAP (
        i_clk       => sysclk,          -- Input clock
        i_rst_n     => reset,           -- Reset signal : synchronous, active low
        o_sync_clk  => sync_clk         -- Output signal used to synchronize the components together
    );

    m_adc : ADC
    GENERIC MAP (
        spi_clk_div => 64               -- SPI clock divider
    )
    PORT MAP (
        clk         => sysclk,
        rst         => reset,
        synclk      => sync_clk,
        adc_cs      => adc_cs,
        adc_clk     => adc_clk,
        adc_mosi    => jc(2),
        adc_miso    => jc(1),
        adc_data    => data_adc
    );
END Behavioral;