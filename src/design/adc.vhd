----------------------------------------------------------------------------------
-- Company: GPSE
-- Engineer: Julien Chapel
-- Create Date: 12/06/2022 10:50:25 AM
-- Design Name: ADC controller
-- Module Name: ADC - Behavioral
-- Project Name: Zybo Dac
-- Project Name: Projet TP
-- Target Devices: Zybo Z7
-- Description: 
--   This is a driver to control the ADC
----------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY ADC IS

    GENERIC (
        spi_clk_div : INTEGER := 64     -- SPI clock divider
    );

    PORT (
        clk      : IN STD_LOGIC;                        -- System clock
        rst      : IN STD_LOGIC;                        -- Reset
        synclk   : IN STD_LOGIC;                        -- Synchronization clock
        adc_cs   : BUFFER STD_LOGIC_VECTOR(0 DOWNTO 0); -- Chip select
        adc_clk  : BUFFER STD_LOGIC;                    -- SPI clock
        adc_mosi : OUT STD_LOGIC;                       -- SPI MOSI
        adc_miso : IN STD_LOGIC;                        -- SPI MISO
        adc_data : OUT STD_LOGIC_VECTOR (23 DOWNTO 0)   -- ADC data
    );

end ADC;

ARCHITECTURE Behavioral OF ADC IS 
    TYPE state_machine is (reset, config, calib, init, waiting, request, reading);     -- state machine type
    SIGNAL state     : state_machine := reset;                              -- state machine
    SIGNAL txdata    : STD_LOGIC_VECTOR (31 DOWNTO 0);                      -- Data to send (4 bytes)
    SIGNAL rxdata    : STD_LOGIC_VECTOR (31 DOWNTO 0);                      -- Data received (4 bytes)
    SIGNAL enable    : STD_LOGIC;                                           -- Enable signal for the SPI component
    SIGNAL s_synclk : STD_LOGIC;                                           -- Synchronization clock shifted by one clock cycle
    SIGNAL busy      : STD_LOGIC;                                           -- Busy signal for the SPI component
    SIGNAL s_busy    : STD_LOGIC;                                           -- Busy signal shifted by one clock cycle 

    CONSTANT AD7193_COMM_REG    : STD_LOGIC_VECTOR (2 DOWNTO 0) := "000";   -- Communication register address
    CONSTANT AD7193_STATUS_REG  : STD_LOGIC_VECTOR (2 DOWNTO 0) := "000";   -- Status register address
    CONSTANT AD7193_MODE_REG    : STD_LOGIC_VECTOR (2 DOWNTO 0) := "001";   -- Mode register address
    CONSTANT AD7193_CONF_REG    : STD_LOGIC_VECTOR (2 DOWNTO 0) := "010";   -- Configuration register address
    CONSTANT AD7193_DATA_REG    : STD_LOGIC_VECTOR (2 DOWNTO 0) := "011";   -- Data register address
    CONSTANT AD7193_ID_REG      : STD_LOGIC_VECTOR (2 DOWNTO 0) := "100";   -- ID register address
    CONSTANT AD7193_GPOCON_REG  : STD_LOGIC_VECTOR (2 DOWNTO 0) := "101";   -- GPOCON register address
    CONSTANT AD7193_OFFSET_REG  : STD_LOGIC_VECTOR (2 DOWNTO 0) := "110";   -- Offset register address
    CONSTANT AD7193_FULLSCA_REG : STD_LOGIC_VECTOR (2 DOWNTO 0) := "111";   -- Full scale register address

    COMPONENT spi_master IS
    GENERIC(
        slaves  : INTEGER;                                      -- Number of spi slaves (here 1 for the DAC)
        d_width : INTEGER                                       -- Data bus width
    );                                     
    PORT(
        clock   : IN     STD_LOGIC;                             -- System clock
        reset_n : IN     STD_LOGIC;                             -- Asynchronous reset
        enable  : IN     STD_LOGIC;                             -- Initiate transaction
        cpol    : IN     STD_LOGIC;                             -- Spi clock polarity
        cpha    : IN     STD_LOGIC;                             -- Spi clock phase
        cont    : IN     STD_LOGIC;                             -- Continuous mode command
        clk_div : IN     INTEGER;                               -- System clock cycles per 1/2 period of sclk
        addr    : IN     INTEGER;                               -- Address of slave
        tx_data : IN     STD_LOGIC_VECTOR(31 DOWNTO 0);         -- Data to transmit
        miso    : IN     STD_LOGIC;                             -- MISO = Master In, Slave Out
        sclk    : BUFFER STD_LOGIC;                             -- Spi clock
        ss_n    : BUFFER STD_LOGIC_VECTOR(slaves-1 DOWNTO 0);   -- Slave select
        mosi    : OUT    STD_LOGIC;                             -- MOSI : Master Out, Slave In
        busy    : OUT    STD_LOGIC;                             -- Busy / data ready signal
        rx_data : OUT    STD_LOGIC_VECTOR(31 DOWNTO 0));        -- Received data
    END COMPONENT spi_master;

BEGIN

    m_spi_master : spi_master
    GENERIC MAP (
        slaves  => 1,
        d_width => 32
    )
    PORT MAP (
        clock   => clk,
        reset_n => rst,
        enable  => enable,
        cpol    => '1',
        cpha    => '1',
        cont    => '0',
        clk_div => spi_clk_div,
        addr    => 0,
        tx_data => txdata,
        miso    => adc_miso,
        sclk    => adc_clk,
        ss_n    => adc_cs,
        mosi    => adc_mosi,
        busy    => busy,
        rx_data => rxdata
    );

    PROCESS (clk, rst)
        -- 500 us counter
        VARIABLE counter : INTEGER RANGE 0 TO 62500;
    BEGIN

        IF (rst = '0') THEN      --Asynchronous Reset

            state   <= reset;
            counter := 0;
            enable  <= '0';
            s_busy  <= '0';
            txdata  <= (OTHERS => '0');
            
        ELSIF (rising_edge(clk)) THEN

            s_synclk <= synclk;
            s_busy <= busy;

            CASE state IS

                WHEN reset =>

                    -- Reset the AD7193 by sending 64 1's
                    IF (counter < 2) THEN
                        IF (busy = '0' AND s_busy = '0') THEN
                            txdata <= (OTHERS => '1');                            
                            enable <= '1';                            
                        ELSIF (busy = '0' AND s_busy = '1') THEN
                            counter := counter + 1;
                        END IF;

                    -- Wait 500 us to let the AD7193 reset
                    ELSIF (counter < 62500) THEN
                        enable <= '0';
                        counter := counter + 1;
                    ELSIF (counter = 62500) THEN
                        counter := 0;
                        state <= config;
                    END IF;

                WHEN config =>

                    -- Write to the AD7193 configuration register
                    -- We need to set the configuration register to 0x000118
                    IF (counter < 1) THEN
                        IF (busy = '0' AND s_busy = '0') THEN
                            -- Create communication word 
                            txdata <= ((("00000" & AD7193_CONF_REG) SLL 3) & x"000118");
                            enable <= '1';
                        ELSIF (busy = '1') THEN
                            enable <= '0';
                            
                        ELSE
                            counter := counter + 1;
                        END IF;

                    -- Wait 5us to let the AD7193 configure
                    ELSIF (counter < 625) THEN
                        counter := counter + 1;
                    
                    -- Write to the AD7193 mode register
                    -- We need to set the mode register to 0x880001
                    -- This will also enable the internal zero scale calibration
                    ELSIF (counter < 626) THEN
                        IF (busy = '0' AND s_busy = '0') THEN
                            -- Create communication word 
                            txdata <= ((("00000" & AD7193_MODE_REG) SLL 3) & x"880001");
                            enable <= '1';
                        ELSIF (busy = '1') THEN
                            enable <= '0';
                        ELSE
                            counter := counter + 1;
                        END IF;
                    
                    -- Wait 500us to let the AD7193 calibrate
                    ELSIF (counter < 62500) THEN
                        counter := counter + 1;
                    ELSE                         
                        counter := 0;
                        state <= calib;
                    END IF;

                WHEN calib =>

                    -- Write to the AD7193 mode register
                    -- We need to set the mode register to 0xA80001
                    -- This will also enable the internal full scale calibration
                    IF (counter < 1) THEN
                        IF (busy = '0' AND s_busy = '0') THEN
                            -- Create communication word 
                            txdata <= ((("00000" & AD7193_MODE_REG) SLL 3) & x"A80001");
                            enable <= '1';
                        ELSIF (busy = '1') THEN
                            enable <= '0';
                        ELSE
                            counter := counter + 1;
                        END IF;

                    -- And let 500us pass
                    ELSIF (counter < 62500) THEN
                        counter := counter + 1;
                    ELSE
                        counter := 0;
                        state <= init;
                    END IF;

                WHEN init =>
                    -- Set the mode register back to continous conversion
                    -- We need to set the mode register to 0x180001
                    IF (counter < 1) THEN
                        IF (busy = '0' AND s_busy = '0') THEN
                            -- Create communication word 
                            txdata <= ((("00000" & AD7193_MODE_REG) SLL 3) & x"180001");
                            enable <= '1';
                        ELSIF (busy = '1') THEN
                            enable <= '0';
                        ELSE
                            counter := counter + 1;
                        END IF;
                    ELSE 
                        counter := 0;
                        state <= waiting;
                    END IF;

                WHEN waiting =>
                        IF (synclk = '1' AND s_synclk = '0') THEN
                            state <= request;
                        END IF;

                WHEN request =>
                    -- In order to read from the data register we need to send
                    -- a read command to the communication register for the data register
                    IF (counter < 1) THEN
                        IF (busy = '0' AND s_busy = '0') THEN
                            -- Create communication word 
                            txdata <= ((("01000" & AD7193_DATA_REG) SLL 3) & x"000000");
                            enable <= '1';
                        ELSIF (busy = '1') THEN
                            enable <= '0';
                        ELSE
                            counter := counter + 1;
                        END IF;
                    
                    -- Wait 5us to let the AD7193 acquire data
                    ELSIF (counter < 625) THEN
                        counter := counter + 1;

                    -- Wait for the ADC to tell us it's ready
                    -- by monitoring the miso pin, a 0 level
                    -- indicates the ADC is ready
                    ELSE
                        IF (adc_miso = '0') THEN
                            counter := 0;
                            state <= reading;
                        END IF;
                    END IF;

                WHEN reading =>
                    -- Read the data register from the AD7193
                    -- We pull low the CS pin and clock in the data

                    IF (busy = '0' AND s_busy = '0') THEN
                        txdata <= (OTHERS => '0');
                        enable <= '1';
                    ELSIF (busy = '1') THEN
                        enable <= '0';
                    ELSE 
                        -- Only keep the 24 LSBs
                        adc_data <= rxdata(23 DOWNTO 0);
                        state <= waiting;
                    END IF;
                    
                WHEN OTHERS =>
                    state <= reset;
                    
            END CASE;
        END IF;
        
    END PROCESS;
END Behavioral;