----------------------------------------------------------------------------------
-- Company: GPSE
-- Engineer: Julien Chapel
-- Create Date: 12/01/2022 09:15:05 AM
-- Design Name: clocky
-- Module Name: clocky - Behavioral
-- Project Name: Zybo Dac
-- Project Name: Projet TP
-- Target Devices: Zybo Z7

-- Description:
-- This component is mainly a clock divider to synchronize the drivers with the
-- rest of the project.
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY clocky IS

    GENERIC (
        target_freq : INTEGER := 4000   -- Frequency in Hz at which a new sample is generated, processed and sent.
    );

    PORT (
        i_clk   : IN STD_LOGIC;         -- Input clock
        i_rst_n : IN STD_LOGIC;         -- Reset signal : Asynchronous, active low
        o_sync_clk   : OUT STD_LOGIC    -- Output signal used to synchronize the components together
    );

END clocky;

ARCHITECTURE Behavioral OF clocky IS

    -- Signals declaration
    SIGNAL temp    : STD_LOGIC;         -- Temporary signal linked to the output of the counter

BEGIN

    -- Process declaration
    PROCESS(i_clk, i_rst_n)
        -- Counter used to generate the sync signal
        VARIABLE counter : INTEGER RANGE 0 TO 125000000 / (4 * target_freq) - 1;     
    BEGIN

        -- Asynchronous reset
        IF (i_rst_n = '0') THEN
            counter := 0;
            temp <= '0';

        -- Increment the counter at each clock edge
        ELSIF (rising_edge(i_clk)) THEN
            counter := counter + 1;

            -- If the counter is equal to the max value,
            IF (counter = 125000000 / (4 * target_freq) - 1) THEN
                counter := 0;           -- ... reset it,
                temp <= NOT temp;       -- ... and switch the output
            END IF;
        END IF;

    END PROCESS;

    -- Output assignment
    o_sync_clk <= temp;

END Behavioral;
