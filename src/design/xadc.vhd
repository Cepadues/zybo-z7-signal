----------------------------------------------------------------------------------
-- Company: GPSE
-- Engineer: Julien Chapel
-- Create Date: 12/06/2022 10:50:25 AM
-- Design Name: ADC controller
-- Module Name: ADC - Behavioral
-- Project Name: Zybo Dac
-- Project Name: Projet TP
-- Target Devices: Zybo Z7
-- Description: 
--   This is a driver to control the ADC
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY ADC IS

    PORT (
        i_clk       : IN STD_LOGIC;                             -- Clock
        i_rst_n     : IN STD_LOGIC;                             -- Reset
        i_sync_clk  : IN STD_LOGIC;                             -- Synchronisation clock
        o_data      : OUT STD_LOGIC_VECTOR (11 DOWNTO 0);       -- Data output
        xa_p        : IN  STD_LOGIC;                            -- XADC channel A positive
        xa_n        : IN  STD_LOGIC                             -- XADC channel A negative
    );
    
END ADC;

ARCHITECTURE Behavioral OF ADC IS
    
    -- XADC IP declaration
    COMPONENT xadc_wiz_0
        PORT (
            daddr_in        : in  STD_LOGIC_VECTOR (6 downto 0);     -- Address bus for the dynamic reconfiguration port
            den_in          : in  STD_LOGIC;                         -- Enable Signal for the dynamic reconfiguration port
            di_in           : in  STD_LOGIC_VECTOR (15 downto 0);    -- Input data bus for the dynamic reconfiguration port
            dwe_in          : in  STD_LOGIC;                         -- Write Enable for the dynamic reconfiguration port
            do_out          : out  STD_LOGIC_VECTOR (15 downto 0);   -- Output data bus for dynamic reconfiguration port
            drdy_out        : out  STD_LOGIC;                        -- Data ready signal for the dynamic reconfiguration port
            dclk_in         : in  STD_LOGIC;                         -- Clock input for the dynamic reconfiguration port
            reset_in        : in  STD_LOGIC;                         -- Reset signal for the System Monitor control logic
            vauxp14         : in  STD_LOGIC;                         -- Auxiliary Channel 14
            vauxn14         : in  STD_LOGIC;
            busy_out        : out  STD_LOGIC;                        -- ADC Busy signal
            channel_out     : out  STD_LOGIC_VECTOR (4 downto 0);    -- Channel Selection Outputs
            eoc_out         : out  STD_LOGIC;                        -- End of Conversion Signal
            eos_out         : out  STD_LOGIC;                        -- End of Sequence Signal
            alarm_out       : out STD_LOGIC;                         -- OR'ed output of all the Alarms
            vp_in           : in  STD_LOGIC;                         -- Dedicated Analog Input Pair
            vn_in           : in  STD_LOGIC
        );
    END COMPONENT;

    SIGNAL daddr_in     : STD_LOGIC_VECTOR (6 DOWNTO 0);
    SIGNAL den_in       : STD_LOGIC;
    SIGNAL data         : STD_LOGIC_VECTOR (11 DOWNTO 0);
    signal data_reg     : STD_LOGIC_VECTOR (15 DOWNTO 0);
    SIGNAL drdy_out     : STD_LOGIC;
    SIGNAL channel_out  : STD_LOGIC_VECTOR (4 DOWNTO 0);
    SIGNAL eoc_out      : STD_LOGIC;

BEGIN

    my_xadc : xadc_wiz_0
        PORT MAP (
            daddr_in    => daddr_in,
            den_in      => den_in,
            di_in       => (OTHERS => '0'),
            dwe_in      => '0',
            do_out      => data_reg,
            drdy_out    => drdy_out,
            dclk_in     => i_clk,
            reset_in    => i_rst_n,
            vauxp14     => xa_p,
            vauxn14     => xa_n,
            busy_out    => open,
            channel_out => channel_out,
            eoc_out     => eoc_out,
            eos_out     => open,
            alarm_out   => open,
            vp_in       => '0',
            vn_in       => '0'
        );

    -- Associate the XADC channel to the output
    o_data <= data;
    
    -- Link the current conversion address to the reading address
    daddr_in <= "00" & channel_out;

    PROCESS(i_clk, i_rst_n)
    BEGIN
        -- Asynchronous reset
        IF (i_rst_n = '0') THEN 

            --daddr_in    <= (OTHERS => '0');
            den_in      <= '0';
            data        <= (OTHERS => '0');

        ELSIF (rising_edge(i_clk)) THEN

            den_in <= eoc_out;          -- Enable the XADC when the conversion is done

            IF (drdy_out = '1') THEN    -- If the data is ready
                -- Since the XADC is a 16-bit ADC, where only the 12 MSB are significants
                -- we need to shift the data to the right and remove the 4 LSB.
                data <= data_reg(15 DOWNTO 4);
            END IF;
        END IF;

    END PROCESS;

END Behavioral;