----------------------------------------------------------------------------------
-- Company: GPSE
-- Engineer: Julien Chapel
-- Create Date: 11/03/2022 02:44:05 PM
-- Design Name: 
-- Module Name: dac - Behavioral
-- Project Name: Zybo Dac
-- Project Name: Projet TP
-- Target Devices: Zybo Z7
-- Description:
--   This module is the DAC module. It is used to generate a voltage on the DAC output.
--   It uses a spi_controller component from Digikey, this allows us to focus on the
--   configuration of the DAC and the user logic.
----------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY dac IS

    GENERIC (
        N            : INTEGER := 16;                           -- Resolution of the DAC
        spi_clk_div  : INTEGER := 512                           -- SPI Clock div, freq_spi = 125MHz / spi_clk_div
    );

    PORT ( 
        i_clk        :  IN STD_LOGIC;                           -- System clock
        i_rst_n      :  IN STD_LOGIC;                           -- Asynchronous reset
        i_sync_clk   :  IN STD_LOGIC;                           -- Enable communication with DAC
        i_data       :  IN STD_LOGIC_VECTOR (N - 1 DOWNTO 0);   -- Data to be loaded in the DAC
        o_spi_clk    :  BUFFER STD_LOGIC;                       -- SPI clock
        o_spi_cs_n   :  BUFFER STD_LOGIC_VECTOR(0 DOWNTO 0);    -- SPI chip select
        o_spi_din    :  OUT STD_LOGIC;                          -- Data sent to the DAC, MOSI
        o_spi_ldac_n :  OUT STD_LOGIC                           -- Load DAC output with the loaded data, replace MISO port
    );
END dac;

ARCHITECTURE Behavioral OF dac IS

    -- Internal signals
    TYPE   state_machine IS (init, waiting, sending, pause);
    SIGNAL state       : state_machine := init;                 -- State machine, maybe we shouldn't init it here
    SIGNAL s_reg       : STD_LOGIC_VECTOR (N - 1 DOWNTO 0);     -- Storing register for the data to be sent
    SIGNAL s_ena       : STD_LOGIC;                             -- Enable signal for the spi_master
    SIGNAL s_sync_temp : STD_LOGIC;                             -- Temporary signal for the syncronization of the dac with other components
    SIGNAL s_busy      : STD_LOGIC;                             -- Busy signal to indicate that the spi_master isn't ready to receive data
    SIGNAL s_busy_temp : STD_LOGIC;                             -- Temporary signal for the busy signal

    -- SPI_master declaration
    COMPONENT spi_master IS
    GENERIC(
        slaves  : INTEGER;                                      -- Number of spi slaves (here 1 for the DAC)
        d_width : INTEGER                                       -- Data bus width
    );                                     
    PORT(
        clock   : IN     STD_LOGIC;                             -- System clock
        reset_n : IN     STD_LOGIC;                             -- Asynchronous reset
        enable  : IN     STD_LOGIC;                             -- Initiate transaction
        cpol    : IN     STD_LOGIC;                             -- Spi clock polarity
        cpha    : IN     STD_LOGIC;                             -- Spi clock phase
        cont    : IN     STD_LOGIC;                             -- Continuous mode command
        clk_div : IN     INTEGER;                               -- System clock cycles per 1/2 period of sclk
        addr    : IN     INTEGER;                               -- Address of slave
        tx_data : IN     STD_LOGIC_VECTOR(d_width-1 DOWNTO 0);  -- Data to transmit
        miso    : IN     STD_LOGIC;                             -- MISO = Master In, Slave Out
        sclk    : BUFFER STD_LOGIC;                             -- Spi clock
        ss_n    : BUFFER STD_LOGIC_VECTOR(slaves-1 DOWNTO 0);   -- Slave select
        mosi    : OUT    STD_LOGIC;                             -- MOSI : Master Out, Slave In
        busy    : OUT    STD_LOGIC;                             -- Busy / data ready signal
        rx_data : OUT    STD_LOGIC_VECTOR(d_width-1 DOWNTO 0)); -- Received data
    END COMPONENT spi_master;

BEGIN

    -- Instantiate the spi_master component
    m_spi_master : spi_master
    GENERIC MAP (
        slaves  => 1,
        d_width => N
    )
    PORT MAP (
        clock   => i_clk,
        reset_n => i_rst_n,
        enable  => s_ena,
        cpol    => '0',
        cpha    => '0',
        cont    => '0',
        clk_div => spi_clk_div,
        addr    => 0,
        tx_data => s_reg,
        miso    => '0',
        sclk    => o_spi_clk,
        ss_n    => o_spi_cs_n,
        mosi    => o_spi_din,
        busy    => s_busy,
        rx_data => open
    );


    PROCESS(i_clk, i_rst_n)
        -- 100 us counter, f_clk = 125 MHz, 100 us = 12500 cycles
        VARIABLE counter : INTEGER RANGE 0 TO 12500;
    BEGIN
        -- Asynchronous Reset
        IF(i_rst_n = '0') THEN
            s_ena           <=  '0';                -- Disable SPI communication with DAC 
            counter         :=   0 ;                -- Reset counter  
            o_spi_ldac_n    <=  '1';                -- Disable DAC output
            s_reg           <=  (OTHERS => '0');    -- Reset the register
            state           <=  init;               -- Reset the state machine
            s_busy_temp     <=  '0';                -- Reset the temporary busy signal
            -- The reset of the rest of the signals will be handled by the spi_controller

        ELSIF(rising_edge(i_clk)) THEN
            s_busy_temp <= s_busy;                  -- Store the current busy signal in a temporary signal
            
            CASE state IS

                -- Initialize the PMOD DA3
                WHEN init =>                                        -- The PMOD DA3 needs 100us to power up            
                    IF (counter = 12500 - 1) THEN                   -- So, at 100us...
                        counter := 0;                               -- Reset the counter...
                        state <= waiting;                           -- And go to the next stage
                    ELSE
                        counter := counter + 1;                     -- Else increment the counter
                    END IF;
                
                -- Wait for the sync signal to trigger the output of the Data
                WHEN waiting =>                                     -- Wait for a rising edge of the sync clock
                    IF (s_sync_temp = '0' AND i_sync_clk = '1') THEN
                        s_reg <= i_data;
                        state <= sending;                           -- Go to the next stage
                    END IF;
                    s_sync_temp <= i_sync_clk;                      -- Store the sync signal in a temporary signal
                
                -- Send the data to the DAC
                WHEN sending =>                                     -- Send the data to the DAC
                    IF (s_busy = '0' AND s_busy_temp = '0') THEN       
                        s_ena <= '1';                               -- Enable SPI communication with DAC
                    ELSIF (s_busy = '1') THEN                       -- If the spi_master is ready to receive data
                        s_ena <= '0';                               -- Disable SPI communication with DAC
                    ELSE 
                        state <= pause;                             -- Wait for the DAC to be ready to receive the next data
                    END IF;

                -- After sending the data, wait for the DAC to be ready to receive the next data
                -- We wait 5 us before loading the new value in the dac.
                -- Then we hold the ldac signal to low for 10 us to load the new value in the dac.
                -- And we pause for 5us before returning to the ready state
                -- f_clk = 125 MHz, 5 us = 625 cycles, 10 us = 1250 cycles
                WHEN pause =>
                    counter := counter + 1;
                    IF (counter > 625 AND counter < 1875) THEN      -- Hold the ldac signal to low for 10 us
                        o_spi_ldac_n <= '0';
                    ELSIF (counter > 2500) THEN                     -- 20us elapsed, go to the next stage
                        counter := 0;                               -- Reset the counter
                        state <= waiting;
                    ELSE 
                        o_spi_ldac_n <= '1';                        -- Keep the ldac signal to high
                    END IF;

                -- If the state machine is in an unknown state, reset it   
                WHEN OTHERS =>
                    state <= init;

            END CASE;
        END IF;
    END PROCESS;

END Behavioral;
